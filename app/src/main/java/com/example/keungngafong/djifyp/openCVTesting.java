package com.example.keungngafong.djifyp;

import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.media.MediaCodec;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class openCVTesting extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {
    private static final String TAG = "CV";
    private CameraBridgeViewBase mOpenCvCameraView;
    private Button mMatch;
    private EditText mGaussian;
    private EditText mH;
    private EditText mHRange;
    private EditText mMinS;
    private EditText mMaxS;
    private EditText mMinV;
    private EditText mMaxV;
    private EditText mH2;
    private EditText mHRange2;
    private EditText mMinS2;
    private EditText mMaxS2;
    private EditText mMinV2;
    private EditText mMaxV2;
    private EditText mFactor;
    private TextView mDist;
    private TextView mXOffset;
    private TextView mYOffset;
    private TextView mTri;
    private TextView mSq;
    private Mat ref;
    private static Mat currentFrame;
    private int count = 0;
    private static boolean refSet = false;
    private double gaussianX = 5;
    private double threshold = 150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_cvtesting);
        Log.i(TAG, "called onCreate");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //if (mIsJavaCamera)
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
        //else
        //mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView2);
        if (!OpenCVLoader.initDebug()) {
        } else {
        }
        mHRange = (EditText) findViewById(R.id.editText_HRange);
        mH = (EditText) findViewById(R.id.editText_H);
        mMinS = (EditText) findViewById(R.id.editText_minS);
        mMaxS = (EditText) findViewById(R.id.editText_maxS);
        mMinV = (EditText) findViewById(R.id.editText_minV);
        mMaxV = (EditText) findViewById(R.id.editText_maxV);
        mHRange2 = (EditText) findViewById(R.id.editText_HRange2);
        mH2 = (EditText) findViewById(R.id.editText_H2);
        mMinS2 = (EditText) findViewById(R.id.editText_minS2);
        mMaxS2 = (EditText) findViewById(R.id.editText_maxS2);
        mMinV2 = (EditText) findViewById(R.id.editText_minV2);
        mMaxV2 = (EditText) findViewById(R.id.editText_maxV2);
        mDist = (TextView) findViewById(R.id.txt_dist);
        mMatch = (Button) findViewById(R.id.btn_match);
        mGaussian = (EditText)findViewById(R.id.edit_Gaussian);
        mXOffset = (TextView) findViewById(R.id.txt_xOffset);
        mYOffset = (TextView) findViewById(R.id.txt_yOffset);
        mSq = (TextView) findViewById(R.id.txt_squareNum);
        mTri = (TextView) findViewById(R.id.txt_triNum);
        mFactor = (EditText)findViewById(R.id.editText_factor);
        mXOffset = (TextView) findViewById(R.id.txt_xOffset);
        mYOffset = (TextView) findViewById(R.id.txt_yOffset);
        mMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mat newFrame = currentFrame.clone();
                int gon = 3;
                Size gaussian = new Size(15, 15);

                Mat yellowFrame = new Mat();
                Mat hsvFrame = new Mat();
                Imgproc.cvtColor(newFrame, newFrame, Imgproc.COLOR_RGBA2BGR);
                Imgproc.cvtColor(newFrame, hsvFrame, Imgproc.COLOR_BGR2HSV);
                double gausFac = Double.parseDouble(mGaussian.getText().toString());
                Imgproc.GaussianBlur(hsvFrame, hsvFrame, gaussian, gausFac);
                Imgproc.cvtColor(newFrame, yellowFrame, Imgproc.COLOR_BGR2HSV);
                Imgproc.GaussianBlur(yellowFrame, yellowFrame, gaussian, gausFac);
                //Imgproc.threshold(yellowFrame, yellowFrame, 150, 255, Imgproc.THRESH_BINARY);
                double hsv_h = Double.parseDouble(mH.getText().toString());
                double hsv_hRange = Double.parseDouble(mHRange.getText().toString());
                double hsv_minS = Double.parseDouble(mMinS.getText().toString());
                double hsv_maxS = Double.parseDouble(mMaxS.getText().toString());
                double hsv_minV = Double.parseDouble(mMinV.getText().toString());
                double hsv_maxV = Double.parseDouble(mMaxV.getText().toString());
                double hsv_h2 = Double.parseDouble(mH2.getText().toString());
                double hsv_hRange2 = Double.parseDouble(mHRange2.getText().toString());
                double hsv_minS2 = Double.parseDouble(mMinS2.getText().toString());
                double hsv_maxS2 = Double.parseDouble(mMaxS2.getText().toString());
                double hsv_minV2 = Double.parseDouble(mMinV2.getText().toString());
                double hsv_maxV2 = Double.parseDouble(mMaxV2.getText().toString());
                Core.inRange(hsvFrame, new Scalar((hsv_h - hsv_hRange) / 2, hsv_minS, hsv_minV), new Scalar((hsv_h + hsv_hRange) / 2, hsv_maxS, hsv_maxV), hsvFrame);
                Core.inRange(yellowFrame, new Scalar((hsv_h2 - hsv_hRange2) / 2, hsv_minS2, hsv_minV2), new Scalar((hsv_h2 + hsv_hRange2) / 2, hsv_maxS2, hsv_maxV2), yellowFrame);
                Mat yellowFrameCop = yellowFrame.clone();
                yellowFrameCop.convertTo(yellowFrameCop, CvType.CV_8UC3);
                List<MatOfPoint2f> detectedSquare = findContourV(hsvFrame, 4);
                List<MatOfPoint> a = new ArrayList();
                mSq.setText(detectedSquare.size()+"");
                for (int i = 0; i < detectedSquare.size(); i++){
                    MatOfPoint approxf1 = new MatOfPoint();
                    detectedSquare.get(i).convertTo(approxf1, CvType.CV_32S);
                    a.add(new MatOfPoint(approxf1));
                }
                List<MatOfPoint2f> detectedPentagon = findContourV(yellowFrame, gon);
                List<MatOfPoint> b = new ArrayList();
                mTri.setText(detectedPentagon.size()+"");
                for (int i = 0; i < detectedPentagon.size(); i++){
                    MatOfPoint approxf1 = new MatOfPoint();
                    detectedPentagon.get(i).convertTo(approxf1, CvType.CV_32S);
                    b.add(new MatOfPoint(approxf1));
                }
                boolean isPenInsideSq = false;
                double pentSize = 0;
                // find if there is any pentagon in square
                double xCenter = 0;
                double yCenter = 0;
                for (int i = 0; i < detectedSquare.size(); i++){
                    List<Point> calPoint = detectedSquare.get(i).toList();
                    double maxX = -1;
                    double minX = Double.MAX_VALUE;
                    double maxY = -1;
                    double minY = Double.MAX_VALUE;
                    for (int ii = 0; ii < 4; ii++) {
                        if (calPoint.get(ii).x > maxX) {
                            maxX = calPoint.get(ii).x;
                        }
                        if (calPoint.get(ii).x < minX) {
                            minX = calPoint.get(ii).x;
                        }
                        if (calPoint.get(ii).y > maxY) {
                            maxY = calPoint.get(ii).y;
                        }
                        if (calPoint.get(ii).y < minY) {
                            minY = calPoint.get(ii).y;
                        }
                    }
                    for (int ii = 0; ii < detectedPentagon.size(); ii++){
                        List<Point> calPointPent = detectedPentagon.get(ii).toList();
                        double maxXPen = -1;
                        double minXPen = Double.MAX_VALUE;
                        double maxYPen = -1;
                        double minYPen = Double.MAX_VALUE;
                        for (int iii = 0; iii < gon; iii++) {
                            if (calPointPent.get(iii).x > maxXPen) {
                                maxXPen = calPointPent.get(iii).x;
                            }
                            if (calPointPent.get(iii).x < minXPen) {
                                minXPen = calPointPent.get(iii).x;
                            }
                            if (calPointPent.get(iii).y > maxYPen) {
                                maxYPen = calPointPent.get(iii).y;
                            }
                            if (calPointPent.get(iii).y < minYPen) {
                                minYPen = calPointPent.get(iii).y;
                            }
                        }
                        if (maxXPen < maxX && maxYPen < maxY && minXPen > minX && minYPen > minY){
                            double newPentSize = Imgproc.contourArea(detectedPentagon.get(ii));
                            if (newPentSize > pentSize) pentSize = newPentSize;
                            double base = Math.sqrt(pentSize * 2.0 * 14.3 / 12.0);
                            double distance = 655*14.3/base;
                            Moments triangleMoment = Imgproc.moments(detectedPentagon.get(ii));
                            int x = (int)(triangleMoment.get_m10()/triangleMoment.get_m00());
                            int y = (int) (triangleMoment.get_m01()/triangleMoment.get_m00());
                            xCenter = (double)x;
                            yCenter = (double)y;
                            double cmPerPixel = 14.3/base;
                            double xOffset = (newFrame.cols()/2 - x)*cmPerPixel;
                            double yOffset = (newFrame.rows()/2 - y)*cmPerPixel;
                            mDist.setText(distance+"");
                            mXOffset.setText(xOffset+"");
                            mYOffset.setText(yOffset+"");
                            break;
                        }
                    }
                }
                // convert to bitmap:

                Bitmap bm = Bitmap.createBitmap(yellowFrame.cols(), yellowFrame.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(yellowFrameCop, bm);
                ImageView iv = (ImageView) findViewById(R.id.imageView);
                iv.setImageBitmap(bm);

                Bitmap bm2 = Bitmap.createBitmap(hsvFrame.cols(), hsvFrame.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(hsvFrame, bm2);
                ImageView iv2 = (ImageView) findViewById(R.id.imageView2);
                iv2.setImageBitmap(bm2);

                Core.line(newFrame, new Point(xCenter, 0), new Point(xCenter, (double) newFrame.rows()), new Scalar(255, 0, 0));
                Core.line(newFrame, new Point(0, yCenter), new Point((double)newFrame.cols(), yCenter), new Scalar(255, 0 , 0));

                Bitmap bm3 = Bitmap.createBitmap(newFrame.cols(), newFrame.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(newFrame, bm3);
                ImageView iv3 = (ImageView) findViewById(R.id.imageView3);
                iv3.setImageBitmap(bm3);
            }
        });

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    private List<MatOfPoint2f> findContourV(Mat image, int vertex){
        Mat tempImage = image.clone();
        Imgproc.Canny(tempImage, tempImage, 30, 90);
        Size kernalSize = new Size (5,5);
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, kernalSize, new Point(1, 1));
        Imgproc.morphologyEx(tempImage, tempImage, Imgproc.MORPH_CLOSE, element);
        List<MatOfPoint> contourPoints = new ArrayList();
        Mat hierachy = new Mat();
        List<MatOfPoint2f> output = new ArrayList();
        Imgproc.findContours(tempImage, contourPoints, hierachy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        double factor = Double.parseDouble(mFactor.getText().toString());
        for (int i = 0; i < contourPoints.size(); i++) {
            MatOfPoint2f point2f = new MatOfPoint2f();
            MatOfPoint2f inputPoint2f = new MatOfPoint2f(contourPoints.get(i).toArray());
            Imgproc.approxPolyDP(inputPoint2f, point2f, Imgproc.arcLength(inputPoint2f, true)*factor, true);
            List<Point> a = point2f.toList();
            int size = a.size();
            if (size == vertex) {
                output.add(point2f);
            }
        }
        return output;
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_10, this, mLoaderCallback);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat newFrame = inputFrame.rgba();
        currentFrame = newFrame;
        return newFrame;
    }
}
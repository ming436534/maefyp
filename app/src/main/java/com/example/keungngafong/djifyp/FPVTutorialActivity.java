package com.example.keungngafong.djifyp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.*;

import dji.sdk.AirLink.DJILBAirLink;
import dji.sdk.Camera.DJICamera;
import dji.sdk.Codec.DJICodecManager;
import dji.sdk.AirLink.DJILBAirLink.DJIOnReceivedVideoCallback;
import dji.sdk.Camera.DJICamera;
import dji.sdk.Camera.DJICamera.CameraReceivedVideoDataCallback;
import dji.sdk.Codec.DJICodecManager;
import dji.sdk.FlightController.DJIFlightController;
import dji.sdk.FlightController.DJIFlightControllerDataType;
import dji.sdk.Products.DJIAircraft;
import dji.sdk.SDKManager.DJISDKManager;
import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIBaseComponent.DJICompletionCallback;
import dji.sdk.base.DJIBaseProduct;
import dji.sdk.base.DJIBaseProduct.Model;
import dji.sdk.base.DJIError;
import dji.sdk.Camera.DJICameraSettingsDef.CameraMode;
import dji.sdk.Camera.DJICameraSettingsDef.CameraShootPhotoMode;


public class FPVTutorialActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener, View.OnClickListener {
    private static final String TAG = FPVTutorialActivity.class.getName();
    private TextureView mVideoSurface = null;
    protected DJICamera.CameraReceivedVideoDataCallback mReceivedVideoDataCallBack = null;
    protected DJILBAirLink.DJIOnReceivedVideoCallback mOnReceivedVideoCallback = null;
    protected DJICodecManager mCodecManager = null;
    private DJIBaseProduct mProduct = null;

    private Button mbtnDebug;
    private Button mVelMode;
    private EditText mH;
    private EditText mHRange;
    private EditText mMinS;
    private EditText mMaxS;
    private EditText mMinV;
    private EditText mMaxV;
    private EditText mH2;
    private EditText mHRange2;
    private EditText mMinS2;
    private EditText mMaxS2;
    private EditText mMinV2;
    private EditText mMaxV2;
    private EditText mKp;
    private EditText mKi;
    private EditText mKd;
    private EditText mFactor;

    private TextView mHeight;
    private TextView mCanDetect;
    private TextView mYOffset;
    private TextView mXOffset;
    private TextView xVel;
    private TextView yVel;
    private TextView mStatus;
    private Handler mHandler;

    private ToggleButton mToggleCV;
    private ToggleButton mToggleAuto;

    private DJIFlightController mFlightController;
    private static boolean isAuto = false;
    private static boolean isFirstTime = true;
    private static Context context;

    float pre_error_X = 0;
    float pre_error_Y = 0;
    float intergral_X = 0;
    float intergral_Y = 0;
    float derivative_X = 0;
    float derivative_Y = 0;
    float Velocity_Y = 0;
    float Velocity_X = 0;
    List<Long> timeRecord;
    List<Double> xOffsetRecord;
    List<Double> yOffsetRecord;
    List<Double> zOffsetRecord;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fpvtutorial);
        mHandler = new Handler();
        timeRecord = new ArrayList();
        xOffsetRecord = new ArrayList();
        yOffsetRecord = new ArrayList();
        zOffsetRecord = new ArrayList();
        context = this.getApplicationContext();
        mReceivedVideoDataCallBack = new CameraReceivedVideoDataCallback() {

            @Override
            public void onResult(byte[] videoBuffer, int size) {
                if (mCodecManager != null) {
                    // Send the raw H264 video data to codec manager for decoding
                    mCodecManager.sendDataToDecoder(videoBuffer, size);
                } else {
                    Log.e(TAG, "mCodecManager is null");
                }
            }
        };

        // The callback for receiving the raw video data from Airlink
        mOnReceivedVideoCallback = new DJIOnReceivedVideoCallback() {

            @Override
            public void onResult(byte[] videoBuffer, int size) {

                if (mCodecManager != null) {
                    // Send the raw H264 video data to codec manager for decoding
                    mCodecManager.sendDataToDecoder(videoBuffer, size);
                }

            }
        };
        mVideoSurface = (TextureView) findViewById(R.id.video_previewer_surface);
        if (null != mVideoSurface) {
            mVideoSurface.setSurfaceTextureListener(this);
        }
        if (!OpenCVLoader.initDebug()) {

        } else {
            //System.loadLibrary("nonfree");
        }
        //btnDebug = (Button) findViewById(R.id.btn_debug);
        /*
        btnDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertSurfaceToMat();
            }
        });
        */
        initUI();
        turnOnVelMode();
    }
    private void turnOnVelMode(){
        mProduct = DJISDKManager.getInstance().getDJIProduct();
        DJIAircraft djiAircraft = (DJIAircraft) DJISDKManager.getInstance().getDJIProduct();
        if ((DjiApplication.getProductInstance() instanceof DJIAircraft) && null != DjiApplication.getProductInstance()) {
            mFlightController = djiAircraft.getFlightController();
            DJIFlightControllerDataType.DJIVirtualStickVerticalControlMode newVerticalControlMode;
            newVerticalControlMode = DJIFlightControllerDataType.DJIVirtualStickVerticalControlMode.Velocity; // Switch to velocity
            mFlightController.setVerticalControlMode(newVerticalControlMode); // Set the new Vertical Control mode.
            DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMode newRollPitchControlMode;
            newRollPitchControlMode = DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMode.Velocity;
            mFlightController.setRollPitchControlMode(newRollPitchControlMode);
            mStatus.setText("Vel mode success");
        } else {
            mStatus.setText("Vel mode fail");
        }
    }

    private void initUI(){


        mHRange = (EditText) findViewById(R.id.editText_HRange);
        mH = (EditText) findViewById(R.id.editText_H);
        mMinS = (EditText) findViewById(R.id.editText_minS);
        mMaxS = (EditText) findViewById(R.id.editText_maxS);
        mMinV = (EditText) findViewById(R.id.editText_minV);
        mMaxV = (EditText) findViewById(R.id.editText_maxV);
        mHRange2 = (EditText) findViewById(R.id.editText_HRange2);
        mH2 = (EditText) findViewById(R.id.editText_H2);
        mMinS2 = (EditText) findViewById(R.id.editText_minS2);
        mMaxS2 = (EditText) findViewById(R.id.editText_maxS2);
        mMinV2 = (EditText) findViewById(R.id.editText_minV2);
        mMaxV2 = (EditText) findViewById(R.id.editText_maxV2);
        mKp = (EditText) findViewById(R.id.editText_Kp);
        mKi = (EditText) findViewById(R.id.editText_Ki);
        mKd = (EditText) findViewById(R.id.editText_Kd);


        mHeight = (TextView) findViewById(R.id.txt_CVheight);
        mXOffset = (TextView) findViewById(R.id.txt_CVxOffset);
        mYOffset = (TextView) findViewById(R.id.txt_CVyOffset);
        mCanDetect = (TextView) findViewById(R.id.txt_CVdetect);
        xVel = (TextView) findViewById(R.id.txt_xV);
        yVel =  (TextView) findViewById(R.id.txt_yV);
        mStatus = (TextView) findViewById(R.id.txt_CVStatus);
        mVelMode = (Button) findViewById(R.id.btn_manualVelMode);
        mbtnDebug = (Button) findViewById(R.id.btn_debug);
        mToggleAuto = (ToggleButton)findViewById(R.id.btnStartAuto);
        mToggleCV = (ToggleButton) findViewById(R.id.btnToggleCV);
        mbtnDebug.setOnClickListener(this);
        mToggleAuto.setOnClickListener(this);
        mToggleCV.setOnClickListener(this);
        mVelMode.setOnClickListener(this);

    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };
    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
        initPreviewer();
        if (mVideoSurface == null) {
            Log.e(TAG, "mVideoSurface is null");
        }
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_10, this, mLoaderCallback);
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        uninitPreviewer();
        if (mHandler != null) {
            mHandler.removeCallbacks(CVProcess);
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    public void onReturn(View view) {
        Log.e(TAG, "onReturn");
        this.finish();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        uninitPreviewer();
        super.onDestroy();
    }

    //
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.e(TAG, "onSurfaceTextureAvailable");
        if (mCodecManager == null) {
            Log.e(TAG, "mCodecManager is null 2");
            mCodecManager = new DJICodecManager(this, surface, width, height);
        }
    }

    //
    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.e(TAG, "onSurfaceTextureSizeChanged");
    }

    //
    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.e(TAG, "onSurfaceTextureDestroyed");
        if (mCodecManager != null) {
            mCodecManager.cleanSurface();
            mCodecManager = null;
        }

        return false;
    }

    //
    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        Log.e(TAG, "onSurfaceTextureUpdated");
    }

    private void initPreviewer() {
        try {
            mProduct = DjiApplication.getProductInstance();
        } catch (Exception exception) {
            mProduct = null;
        }

        if (null == mProduct || !mProduct.isConnected()) {

        } else {


            if (null != mVideoSurface) {
                mVideoSurface.setSurfaceTextureListener(this);
            }
            if (!mProduct.getModel().equals(Model.UnknownAircraft)) {
            } else {
                if (null != mProduct.getAirLink()) {
                    if (null != mProduct.getAirLink().getLBAirLink()) {
                        // Set the callback
                        mProduct.getAirLink().getLBAirLink().setDJIOnReceivedVideoCallback(mOnReceivedVideoCallback);
                        turnOnVelMode();
                    }
                }
            }
        }

    }

    private void uninitPreviewer() {
        try {
            mProduct = DjiApplication.getProductInstance();
        } catch (Exception exception) {
            mProduct = null;
        }

        if (null == mProduct || !mProduct.isConnected()) {
        } else {
            if (!mProduct.getModel().equals(Model.UnknownAircraft)) {
            } else {
                if (null != mProduct.getAirLink()) {
                    if (null != mProduct.getAirLink().getLBAirLink()) {
                        // Set the callback
                        mProduct.getAirLink().getLBAirLink().setDJIOnReceivedVideoCallback(null);
                    }
                }
            }
        }
    }
    private void debug(){
/*
        Mat newFrame = new Mat();
        Utils.bitmapToMat(mVideoSurface.getBitmap(), newFrame);
        int gon = 3;
        Size gaussian = new Size(15, 15);
        Mat yellowFrame = new Mat();
        Mat hsvFrame = new Mat();
        Imgproc.cvtColor(newFrame, newFrame, Imgproc.COLOR_RGBA2BGR);
        Imgproc.cvtColor(newFrame, hsvFrame, Imgproc.COLOR_BGR2HSV);
        Imgproc.GaussianBlur(hsvFrame, hsvFrame, gaussian, 2);
        Imgproc.cvtColor(newFrame, yellowFrame, Imgproc.COLOR_BGR2HSV);
        Imgproc.GaussianBlur(yellowFrame, yellowFrame, gaussian, 2);
        //Imgproc.threshold(yellowFrame, yellowFrame, 150, 255, Imgproc.THRESH_BINARY);
        double hsv_h = Double.parseDouble(mH.getText().toString());
        double hsv_hRange = Double.parseDouble(mHRange.getText().toString());
        double hsv_minS = Double.parseDouble(mMinS.getText().toString());
        double hsv_maxS = Double.parseDouble(mMaxS.getText().toString());
        double hsv_minV = Double.parseDouble(mMinV.getText().toString());
        double hsv_maxV = Double.parseDouble(mMaxV.getText().toString());
        double hsv_h2 = Double.parseDouble(mH2.getText().toString());
        double hsv_hRange2 = Double.parseDouble(mHRange2.getText().toString());
        double hsv_minS2 = Double.parseDouble(mMinS2.getText().toString());
        double hsv_maxS2 = Double.parseDouble(mMaxS2.getText().toString());
        double hsv_minV2 = Double.parseDouble(mMinV2.getText().toString());
        double hsv_maxV2 = Double.parseDouble(mMaxV2.getText().toString());
        Core.inRange(hsvFrame, new Scalar((hsv_h - hsv_hRange) / 2, hsv_minS, hsv_minV), new Scalar((hsv_h + hsv_hRange) / 2, hsv_maxS, hsv_maxV), hsvFrame);
        Core.inRange(yellowFrame, new Scalar((hsv_h2 - hsv_hRange2) / 2, hsv_minS2, hsv_minV2), new Scalar((hsv_h2 + hsv_hRange2) / 2, hsv_maxS2, hsv_maxV2), yellowFrame);
        Mat yellowFrameCop = yellowFrame.clone();
        yellowFrameCop.convertTo(yellowFrameCop, CvType.CV_8UC3);
        List<MatOfPoint2f> detectedSquare = findContourV(hsvFrame, 4);
        List<MatOfPoint2f> detectedPentagon = findContourV(yellowFrame, gon);
        boolean isPenInsideSq = false;
        double pentSize = 0;
        // find if there is any pentagon in square
        for (int i = 0; i < detectedSquare.size(); i++){
            List<Point> calPoint = detectedSquare.get(i).toList();
            double maxX = -1;
            double minX = Double.MAX_VALUE;
            double maxY = -1;
            double minY = Double.MAX_VALUE;
            for (int ii = 0; ii < 4; ii++) {
                if (calPoint.get(ii).x > maxX) {
                    maxX = calPoint.get(ii).x;
                }
                if (calPoint.get(ii).x < minX) {
                    minX = calPoint.get(ii).x;
                }
                if (calPoint.get(ii).y > maxY) {
                    maxY = calPoint.get(ii).y;
                }
                if (calPoint.get(ii).y < minY) {
                    minY = calPoint.get(ii).y;
                }
            }
            for (int ii = 0; ii < detectedPentagon.size(); ii++){
                List<Point> calPointPent = detectedPentagon.get(ii).toList();
                double maxXPen = -1;
                double minXPen = Double.MAX_VALUE;
                double maxYPen = -1;
                double minYPen = Double.MAX_VALUE;
                for (int iii = 0; iii < gon; iii++) {
                    if (calPointPent.get(iii).x > maxXPen) {
                        maxXPen = calPointPent.get(iii).x;
                    }
                    if (calPointPent.get(iii).x < minXPen) {
                        minXPen = calPointPent.get(iii).x;
                    }
                    if (calPointPent.get(iii).y > maxYPen) {
                        maxYPen = calPointPent.get(iii).y;
                    }
                    if (calPointPent.get(iii).y < minYPen) {
                        minYPen = calPointPent.get(iii).y;
                    }
                }
                if (maxXPen < maxX && maxYPen < maxY && minXPen > minX && minYPen > minY){
                    double newPentSize = Imgproc.contourArea(detectedPentagon.get(ii));
                    if (newPentSize > pentSize) pentSize = newPentSize;
                    double base = Math.sqrt(pentSize * 2.0 * 14.3 / 12.0);
                    double distance = 606.202797203*14.3/base;
                    Moments triangleMoment = Imgproc.moments(detectedPentagon.get(ii));
                    int y = (int) (triangleMoment.get_m10()/triangleMoment.get_m00());
                    int x = (int) (triangleMoment.get_m01()/triangleMoment.get_m00());
                    double cmPerPixel = 14.3/base;
                    double xOffset = (newFrame.rows()/2 - x)*cmPerPixel/100;
                    double yOffset = -(newFrame.cols()/2 - y)*cmPerPixel/100;
                    mHeight.setText(distance+"");
                    mXOffset.setText(xOffset+"");
                    mYOffset.setText(yOffset+"");
                    break;
                }
            }
        }
        // convert to bitmap:

        Bitmap bm = Bitmap.createBitmap(yellowFrame.cols(), yellowFrame.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(yellowFrameCop, bm);
        ImageView iv = (ImageView) findViewById(R.id.imageView);
        iv.setImageBitmap(bm);

        Bitmap bm2 = Bitmap.createBitmap(hsvFrame.cols(), hsvFrame.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(hsvFrame, bm2);
        ImageView iv2 = (ImageView) findViewById(R.id.imageView2);
        iv2.setImageBitmap(bm2);
        */
        mHandler.post(printTxtFile);
    }

    private List<MatOfPoint2f> findContourV(Mat image, int vertex){
        Mat tempImage = image.clone();
        Imgproc.Canny(tempImage, tempImage, 30, 90);
        List<MatOfPoint> contourPoints = new ArrayList();
        Mat hierachy = new Mat();
        List<MatOfPoint2f> output = new ArrayList();
        Imgproc.findContours(tempImage, contourPoints, hierachy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        for (int i = 0; i < contourPoints.size(); i++) {
            MatOfPoint2f point2f = new MatOfPoint2f();
            MatOfPoint2f inputPoint2f = new MatOfPoint2f(contourPoints.get(i).toArray());
            Imgproc.approxPolyDP(inputPoint2f, point2f, Imgproc.arcLength(inputPoint2f, true)*0.1, true);
            List<Point> a = point2f.toList();
            int size = a.size();
            if (size == vertex) {
                output.add(point2f);
            }
        }
        return output;
    }
    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.btn_debug:
                debug();
                break;
            case R.id.btnStartAuto:
                onClick_btnStartAuto();
                break;
            case R.id.btnToggleCV:
                onClick_btnToggleCV();
                break;
            case R.id.btn_manualVelMode:
                turnOnVelMode();

        }
    }

    public void onClick_btnToggleCV(){
        if (mToggleCV.isChecked()){
            mHandler.post(CVProcess);
        }else{
            mHandler.removeCallbacks(CVProcess);
        }
    }
    public void onClick_btnStartAuto(){
        if (mToggleAuto.isChecked()){
            mProduct = DJISDKManager.getInstance().getDJIProduct();
            DJIAircraft djiAircraft = (DJIAircraft) DJISDKManager.getInstance().getDJIProduct();
            if ((DjiApplication.getProductInstance() instanceof DJIAircraft) && null != DjiApplication.getProductInstance()) {
                mFlightController = djiAircraft.getFlightController();
                if (mFlightController.isVirtualStickControlModeAvailable()) {
                    mFlightController.enableVirtualStickControlMode(new DJIBaseComponent.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError == null) {
                                isAuto = true;
                                isFirstTime = true;
                            } else {
                                isAuto = false;
                                isFirstTime = false;
                                mToggleAuto.setChecked(false);
                            }
                        }
                    });
                } else {
                }
            }
        }else{
            DJIAircraft djiAircraft = DjiApplication.getAircraftInstance();
            if ((DjiApplication.getProductInstance() instanceof DJIAircraft) && null != DjiApplication.getProductInstance()) {
                mFlightController = djiAircraft.getFlightController();
                if (mFlightController.isVirtualStickControlModeAvailable()) {
                    mFlightController.disableVirtualStickControlMode(new DJIBaseComponent.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError == null) {
                                isAuto = false;
                                isFirstTime = true;
                            } else {
                            }
                        }
                    });
                } else {
                }
            }
            mHandler.post(printTxtFile);
        }
    }

    final Runnable printTxtFile = new Runnable(){
        public void run(){
            File path = context.getExternalFilesDir(null);
            String fileName = "kp_"+mKp.getText().toString()+"_ki_"+mKi.getText().toString()+"_kd_"+mKd.getText().toString()+"_"+Calendar.getInstance().getTime();
            File file = new File(path, fileName);
            try{
                FileOutputStream stream = new FileOutputStream(file);
                try {
                    /*
                    String printStr = "time"+ "\t" + "xOffset" + "\t" +"yOffset" + "\t" + "zOffset" + "\n";
                    for (int t = 0; t < timeRecord.size(); t++){
                        printStr += timeRecord.get(t)+"\t"+xOffsetRecord.get(t)+"\t"+yOffsetRecord.get(t)+"\t"+zOffsetRecord.get(t)+"\n";
                    }
                    */
                    String printStr = "diu7u";
                    stream.write(printStr.getBytes());
                    stream.close();
                } catch (IOException e){

                }
            } catch (FileNotFoundException e) {

            }
        }
    };

    final Runnable CVProcess = new Runnable(){
        public void run(){
            Mat newFrame = new Mat();
            Utils.bitmapToMat(mVideoSurface.getBitmap(), newFrame);
            int gon = 3;
            Size gaussian = new Size(15, 15);
            Imgproc.GaussianBlur(newFrame, newFrame, gaussian, 2);
            Mat yellowFrame = new Mat();
            Mat hsvFrame = new Mat();
            Imgproc.cvtColor(newFrame, newFrame, Imgproc.COLOR_RGBA2BGR);
            Imgproc.cvtColor(newFrame, hsvFrame, Imgproc.COLOR_BGR2HSV);
            Imgproc.cvtColor(newFrame, yellowFrame, Imgproc.COLOR_BGR2HSV);
            //Imgproc.threshold(yellowFrame, yellowFrame, 150, 255, Imgproc.THRESH_BINARY);
            double hsv_h = Double.parseDouble(mH.getText().toString());
            double hsv_hRange = Double.parseDouble(mHRange.getText().toString());
            double hsv_minS = Double.parseDouble(mMinS.getText().toString());
            double hsv_maxS = Double.parseDouble(mMaxS.getText().toString());
            double hsv_minV = Double.parseDouble(mMinV.getText().toString());
            double hsv_maxV = Double.parseDouble(mMaxV.getText().toString());
            double hsv_h2 = Double.parseDouble(mH2.getText().toString());
            double hsv_hRange2 = Double.parseDouble(mHRange2.getText().toString());
            double hsv_minS2 = Double.parseDouble(mMinS2.getText().toString());
            double hsv_maxS2 = Double.parseDouble(mMaxS2.getText().toString());
            double hsv_minV2 = Double.parseDouble(mMinV2.getText().toString());
            double hsv_maxV2 = Double.parseDouble(mMaxV2.getText().toString());
            float kp = Float.parseFloat(mKp.getText().toString());
            float ki = Float.parseFloat(mKi.getText().toString());
            float kd = Float.parseFloat(mKd.getText().toString());

            Core.inRange(hsvFrame, new Scalar((hsv_h - hsv_hRange) / 2, hsv_minS, hsv_minV), new Scalar((hsv_h + hsv_hRange) / 2, hsv_maxS, hsv_maxV), hsvFrame);
            Core.inRange(yellowFrame, new Scalar((hsv_h2 - hsv_hRange2) / 2, hsv_minS2, hsv_minV2), new Scalar((hsv_h2 + hsv_hRange2) / 2, hsv_maxS2, hsv_maxV2), yellowFrame);
            Mat yellowFrameCop = yellowFrame.clone();
            yellowFrameCop.convertTo(yellowFrameCop, CvType.CV_8UC3);
            List<MatOfPoint2f> detectedSquare = findContourV(hsvFrame, 4);
            List<MatOfPoint2f> detectedPentagon = findContourV(yellowFrame, gon);
            boolean isDetected = false;
            double pentSize = 0;
            int PentIndex = 0;
            // find if there is any pentagon in square
            for (int i = 0; i < detectedSquare.size(); i++){
                List<Point> calPoint = detectedSquare.get(i).toList();
                double maxX = -1;
                double minX = Double.MAX_VALUE;
                double maxY = -1;
                double minY = Double.MAX_VALUE;
                for (int ii = 0; ii < 4; ii++) {
                    if (calPoint.get(ii).x > maxX) {
                        maxX = calPoint.get(ii).x;
                    }
                    if (calPoint.get(ii).x < minX) {
                        minX = calPoint.get(ii).x;
                    }
                    if (calPoint.get(ii).y > maxY) {
                        maxY = calPoint.get(ii).y;
                    }
                    if (calPoint.get(ii).y < minY) {
                        minY = calPoint.get(ii).y;
                    }
                }
                for (int ii = 0; ii < detectedPentagon.size(); ii++){
                    List<Point> calPointPent = detectedPentagon.get(ii).toList();
                    double maxXPen = -1;
                    double minXPen = Double.MAX_VALUE;
                    double maxYPen = -1;
                    double minYPen = Double.MAX_VALUE;
                    for (int iii = 0; iii < gon; iii++) {
                        if (calPointPent.get(iii).x > maxXPen) {
                            maxXPen = calPointPent.get(iii).x;
                        }
                        if (calPointPent.get(iii).x < minXPen) {
                            minXPen = calPointPent.get(iii).x;
                        }
                        if (calPointPent.get(iii).y > maxYPen) {
                            maxYPen = calPointPent.get(iii).y;
                        }
                        if (calPointPent.get(iii).y < minYPen) {
                            minYPen = calPointPent.get(iii).y;
                        }
                    }
                    if (maxXPen < maxX && maxYPen < maxY && minXPen > minX && minYPen > minY){
                        double newPentSize = Imgproc.contourArea(detectedPentagon.get(ii));
                        if (newPentSize > pentSize) {
                            pentSize = newPentSize;
                            PentIndex = ii;
                        }
                        isDetected = true;
                    }
                }
            }
            if (isDetected){
                double base = Math.sqrt(pentSize * 2.0 * 14.3 / 12.0);
                double distance = 606.202797203*14.3/base/100;
                Moments triangleMoment = Imgproc.moments(detectedPentagon.get(PentIndex));
                int y = (int) (triangleMoment.get_m10()/triangleMoment.get_m00());
                int x = (int) (triangleMoment.get_m01()/triangleMoment.get_m00());
                double cmPerPixel = 14.3/base;
                double xOffset = (newFrame.rows()/2 - x)*cmPerPixel/100;
                double yOffset = -(newFrame.cols()/2 - y)*cmPerPixel/100;
                mHeight.setText(distance+"");
                mXOffset.setText(xOffset+"");
                mYOffset.setText(yOffset+"");
                if (isAuto) {
                    if(isFirstTime){
                        pre_error_X = (float) xOffset;
                        pre_error_Y = (float) yOffset;
                        count = 0;
                        xOffsetRecord.clear();
                        yOffsetRecord.clear();
                        zOffsetRecord.clear();
                        timeRecord.clear();
                        isFirstTime = false;
                    }
                    xOffsetRecord.add(xOffset);
                    yOffsetRecord.add(yOffset);
                    zOffsetRecord.add(distance);
                    timeRecord.add(Calendar.getInstance().getTimeInMillis());
                    PIDControl((float) xOffset, (float) yOffset, kp, ki, kd);
                    //PDControl((float) xOffset, (float) yOffset, kp, kd);
                }
            }
            mHandler.postDelayed(CVProcess, 200);
        }
    };

    void PControl(float Target_X_value, float Target_Y_value){
        if(Math.abs(Target_X_value)< 0.1 && Math.abs(Target_Y_value) < 0.1){
            isAuto = false;
            mFlightController.autoLanding(new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    if (djiError != null) {
                    } else {
                    }
                }
            });
        }else{
            float flVelocity_X = 0;
            float flVelocity_Y = 0;
            float flYaw = 0;
            float flThrottle = 0;
            if (mFlightController.getRollPitchControlMode() == DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMode.Velocity)  {

                //float factor = Float.parseFloat(mFactor.toString());
                flVelocity_Y =  ((Target_Y_value)/2.0f); // This is the pitch velocity value
                flVelocity_X =  ((Target_X_value)/2.0f); // This is the roll velocity value

                if(Math.abs(Target_X_value) < 0.1)
                {
                    flVelocity_X = 0;
                }
                if(Math.abs(Target_Y_value) < 0.1)
                {
                    flVelocity_Y = 0;
                }
            }
            DJIFlightControllerDataType.DJIVirtualStickFlightControlData controlData = new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(flVelocity_X, -flVelocity_Y, flYaw, flThrottle);
            // Using the final string for Throttle, Pitch, Roll, and Yaw is a hack and not appropriate coding. Need to use a proper class instead.

            mFlightController.sendVirtualStickFlightControlData(controlData, new DJIBaseComponent.DJICompletionCallback() { // Send the control data command to the drone.
                @Override
                public void onResult(DJIError djiError) {
                    if (djiError == null) {

                    } else {

                    }
                }
            });
        }

    }

    void PIDControl(float Target_X_value, float Target_Y_value, float Kp,  float Ki, float Kd){
        if(Math.abs(Target_X_value)< 0.1 && Math.abs(Target_Y_value) < 0.1){
            count++;
            if(count == 5) {
                isAuto = false;
                mFlightController.autoLanding(new DJIBaseComponent.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError != null) {
                        } else {
                        }
                    }
                });
            }
        }else{
            float flVelocity_X = 0;
            float flVelocity_Y = 0;
            float flYaw = 0;
            float flThrottle = 0;

            float fil = 0.5f;

            float error_X = Target_X_value;
            float error_Y = Target_Y_value;

            float pre_Velocity_Y = Velocity_Y;
            float pre_Velocity_X = Velocity_X;

            if(Math.abs(Target_X_value)< 0.2 && Math.abs(Target_Y_value) < 0.2) {
                intergral_X = intergral_X + error_X;
                intergral_Y = intergral_Y + error_Y;
            }
            else
            {
                intergral_X = 0;
                intergral_Y = 0;
            }

            derivative_X = (error_X - pre_error_X);
            derivative_Y = (error_Y - pre_error_Y);

            Velocity_X = (Kp * error_X) + (Ki * intergral_X) + (Kd * derivative_X);
            Velocity_Y = (Kp * error_Y) + (Ki * intergral_Y) + (Kd * derivative_Y);

            pre_error_X = error_X;
            pre_error_Y = error_Y;

            Velocity_Y = fil * Velocity_Y + (1 - fil ) * pre_Velocity_Y;
            Velocity_X = fil * Velocity_X + (1 - fil) * pre_Velocity_X;

            pre_Velocity_Y = Velocity_Y;
            pre_Velocity_X = Velocity_X;

            if (mFlightController.getRollPitchControlMode() == DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMode.Velocity)  {

                //float factor = Float.parseFloat(mFactor.toString());
                flVelocity_Y =  Velocity_Y; // This is the pitch velocity value
                flVelocity_X =  Velocity_X; // This is the roll velocity value

                if(Math.abs(Target_X_value) < 0.1)
                {
                    flVelocity_X = 0;
                }
                if(Math.abs(Target_Y_value) < 0.1)
                {
                    flVelocity_Y = 0;
                }
            }
            xVel.setText(flVelocity_Y+"");
            yVel.setText(flVelocity_X+"");
            DJIFlightControllerDataType.DJIVirtualStickFlightControlData controlData = new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(flVelocity_Y, flVelocity_X, flYaw, flThrottle);
            // Using the final string for Throttle, Pitch, Roll, and Yaw is a hack and not appropriate coding. Need to use a proper class instead.

            mFlightController.sendVirtualStickFlightControlData(controlData, new DJIBaseComponent.DJICompletionCallback() { // Send the control data command to the drone.
                @Override
                public void onResult(DJIError djiError) {
                    if (djiError == null) {

                    } else {

                    }
                }
            });
        }

    }

    void PDControl(float Target_X_value, float Target_Y_value, float Kp, float Kd){
        if(Math.abs(Target_X_value)< 0.1 && Math.abs(Target_Y_value) < 0.1){
            isAuto = false;
            mFlightController.autoLanding(new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    if (djiError != null) {
                    } else {
                    }
                }
            });
        }else{
            float flVelocity_X = 0;
            float flVelocity_Y = 0;
            float flYaw = 0;
            float flThrottle = 0;

            float fil = 0.5f;

            float error_X = Target_X_value;
            float error_Y = Target_Y_value;


            float pre_Velocity_Y = Velocity_Y;
            float pre_Velocity_X = Velocity_X;

            intergral_X = intergral_X + error_X;
            intergral_Y = intergral_Y + error_Y;

            derivative_X = (error_X - pre_error_X);
            derivative_Y = (error_Y - pre_error_Y);

//            Velocity_Y = (Kp_X * error_X) + (Ki_X * intergral_X) + (Kd_X * derivative_X);
//            Velocity_X = (Kp_Y * error_Y) + (Ki_Y * intergral_Y) + (Kd_Y * derivative_Y);

            Velocity_X = (Kp * error_X) + (Kd * derivative_X);
            Velocity_Y = (Kp * error_Y)  + (Kd * derivative_Y);

            pre_error_X = error_X;
            pre_error_Y = error_Y;

            Velocity_Y = fil * Velocity_Y + (1 - fil ) * pre_Velocity_Y;
            Velocity_X = fil * Velocity_X + (1 - fil) * pre_Velocity_X;

            pre_Velocity_Y = Velocity_Y;
            pre_Velocity_X = Velocity_X;

            if (mFlightController.getRollPitchControlMode() == DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMode.Velocity)  {

                //float factor = Float.parseFloat(mFactor.toString());
                flVelocity_Y =  Velocity_Y; // This is the pitch velocity value
                flVelocity_X =  Velocity_X; // This is the roll velocity value

                if(Math.abs(Target_X_value) < 0.1)
                {
                    flVelocity_X = 0;
                }
                if(Math.abs(Target_Y_value) < 0.1)
                {
                    flVelocity_Y = 0;
                }
            }
            xVel.setText(flVelocity_Y+"");
            yVel.setText(flVelocity_X+"");
            DJIFlightControllerDataType.DJIVirtualStickFlightControlData controlData = new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(flVelocity_Y, flVelocity_X, flYaw, flThrottle);
            // Using the final string for Throttle, Pitch, Roll, and Yaw is a hack and not appropriate coding. Need to use a proper class instead.

            mFlightController.sendVirtualStickFlightControlData(controlData, new DJIBaseComponent.DJICompletionCallback() { // Send the control data command to the drone.
                @Override
                public void onResult(DJIError djiError) {
                    if (djiError == null) {

                    } else {

                    }
                }
            });
        }

    }

}

package com.example.keungngafong.djifyp;

import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import dji.sdk.Products.DJIAircraft;
import dji.sdk.SDKManager.DJISDKManager;
import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIError;

public class SwitchboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switchboard);

        // Set up a fragment manager to add in ground station capabilities.
        // Each fragment will be one capability: ie. battery, flight controller, video...
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        BatteryFragment(ft);
        FlightControllerCurrentStateFragment(ft);
        ft.commit();
    }

    private void BatteryFragment(FragmentTransaction ft) {
        BatteryFragment batteryFragment = new BatteryFragment();
        ft.add(R.id.fragmentContainer, batteryFragment, "BatteryFragment");
    }

    private void FlightControllerCurrentStateFragment(FragmentTransaction ft) {
        FlightControllerCurrentStateFragment flightControllerCurrentStateFragment = new FlightControllerCurrentStateFragment();
        ft.add(R.id.fragmentContainer, flightControllerCurrentStateFragment, "FlightControllerCurrentStateFragment");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_switchboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
